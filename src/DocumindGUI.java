import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;

import org.apache.poi.hdf.extractor.WordDocument;
import org.apache.poi.hdf.model.hdftypes.FileInformationBlock;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.pdfbox.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.util.PDFTextStripper;

//import quicktime.app.event.MouseButtonListener;

import com.sun.xml.internal.stream.util.BufferAllocator;

import LBJ2.nlp.Word;
import LBJ2.nlp.seg.Token;

import edu.illinois.cs.cogcomp.lbj.pos.POSTagPlain;

public class DocumindGUI {
	//GUI Components Global Variables
	private JFrame frame;
	private Container container;
	private JTextArea documentArea;
	private JScrollPane scrollPane;
	private JButton getHighlightedButton;
	private JPanel resourcesPanel;
	private JScrollPane scrollPaneSummary;
	private JButton openFileButton;
	private JButton rankHighlightedButton;
	private JButton textRankButton;
	private JButton generateQueryButton;
	private JButton generateSelectQueryButton;
	private JList list;
	private JScrollPane listScroller;
	private JLabel queryLabel;

	//Data global variables
	private DefaultListModel model;	
	private File curFile;
	private String curQuery;
	private Highlighter highlighter;
	
	//Contants
	private static final int FRAME_HEIGHT = 500;
	private static final int FRAME_WIDTH = 500;

	URI uri1;
	URI uri2;
	URI uri3;
	URI uri4;
	
	/**
	 * ButtonListener for all resources button at bottom of application
	 * @author Chris
	 *
	 */
	class ResourcesButtonListener implements ActionListener {
		ResourcesButtonListener() {
		  }

		  //Add what happens to when buttons are pressed here
		  public void actionPerformed(ActionEvent e) {
		    if (e.getActionCommand().equals("Button Name")) {
		    	//call helper function to do something
		    }
		    else if(e.getActionCommand().equals("Different button Name")) {
		    	//call other helper funciton to do something else
		    }
		 }
	}
	
	
	
	
	//Add buttons here to the jPanel called: resourcesPanel
	private void addButtonsToJPanel() throws IOException, URISyntaxException {
		/*
		 * 1) Change numOfButtons to the # of buttons you add
		 * 2) Make the buttons global variables like the others above 
		 * 3) Set their actionlisteners to: button.addActionListener(new ResourcesButtonListener());
		 */
		
		
	    class OpenUrlAction1 implements ActionListener {
	      @Override public void actionPerformed(ActionEvent e) {
	    	  try {
	    		  generateQueryFromSelected();
				Desktop.getDesktop().browse(uri1);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	      }
	    }
	    
	    class OpenUrlAction2 implements ActionListener {
		      @Override public void actionPerformed(ActionEvent e) {
		    	  try {
		    		  generateQueryFromSelected();
					Desktop.getDesktop().browse(uri2);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		      }
		    }
	    
	    
	    class OpenUrlAction3 implements ActionListener {
		      @Override public void actionPerformed(ActionEvent e) {
		    	  try {
		    		  generateQueryFromSelected();
					Desktop.getDesktop().browse(uri3);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		      }
		    }
	    
	    
	    class OpenUrlAction4 implements ActionListener {
		      @Override public void actionPerformed(ActionEvent e) {
		    	  try {
		    		  generateQueryFromSelected();
					Desktop.getDesktop().browse(uri4);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		      }
		    }
		
		
		int numOfButtons = 4;
		
		Image googleLogoLarge = ImageIO.read(getClass().getResource("/google.png"));
		Image googleLogo = googleLogoLarge.getScaledInstance( 360, 140,  java.awt.Image.SCALE_SMOOTH ) ;
		BufferedImage googleScholarLogo = ImageIO.read(getClass().getResource("/Scholar.png"));
		BufferedImage youtubeLogo = ImageIO.read(getClass().getResource("/youtube.png"));
		BufferedImage MSAcademicLogo = ImageIO.read(getClass().getResource("/ms_academic.png"));
		
		resourcesPanel.setLayout(new GridLayout(0, numOfButtons));
		
		JButton google = new JButton();
		google.setIcon(new ImageIcon(googleLogo));
		
		JButton googleScholar = new JButton();
		googleScholar.setIcon(new ImageIcon(googleScholarLogo));
		
		JButton youtube = new JButton();
		youtube.setIcon(new ImageIcon(youtubeLogo));
		
		JButton MSAcademic = new JButton();
		MSAcademic.setIcon(new ImageIcon(MSAcademicLogo));
		
		resourcesPanel.add(google);
		resourcesPanel.add(googleScholar);
		resourcesPanel.add(youtube);
		resourcesPanel.add(MSAcademic);
		google.addActionListener(new OpenUrlAction1());
		googleScholar.addActionListener(new OpenUrlAction2());
		youtube.addActionListener(new OpenUrlAction3());
		MSAcademic.addActionListener(new OpenUrlAction4());
		
	}

	/**
	 * Gets the keywords from the text, either highlighted text or all text, via a call to
	 * TextRank.runTextRank(). 
	 */
	public void getKeywordsFromText(boolean highlighted) {
		String text = "";
		if(highlighted)
			text = documentArea.getSelectedText();
		else
			text = documentArea.getText();
		model.clear();
		if(text != "") {
			String lines[] = text.split("\\.");
			ArrayList<KeyPhraseNode> keyPhrases = TextRank.runTextRank(lines);
			for(KeyPhraseNode node : keyPhrases){
				System.out.println(node.phrase + " - " + node.score);
				model.addElement(node.phrase);
			}
		}
	}
	
	/**
	 * Prompts user to open file and calls helper functions to handle setting up text area
	 */
	public void openFile(){
		JFileChooser fileopen = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Acceptable Files", "txt", "doc", "docx", "pdf");
		fileopen.setFileFilter(filter);
		
		int ret = fileopen.showDialog(frame, "Select File");
		if(ret == JFileChooser.APPROVE_OPTION){
			try {
				documentArea.setText("");
				curFile = fileopen.getSelectedFile();
				String extension = getExtension(curFile);
				if(extension.contains("doc")) 
					openWordFile(extension);
				else if(extension.contains("pdf"))
					openPDFFile();
				else{
					FileReader reader = new FileReader(curFile.getPath());
					BufferedReader br = new BufferedReader(reader);
					ArrayList<String> tempList = new ArrayList<String>();
					reader = new FileReader(curFile.getPath());
					br.close();
					br = new BufferedReader(reader);
					documentArea.read(br, null);
					br.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else{
			System.out.println("Invalid file");
		}
	}
	
	
	/**
	 * Sets up documentArea based on word document that was opened
	 * @param extension
	 */
	public void openWordFile(String extension){	
		try {
			documentArea.setText("");
			FileInputStream fs;
			fs = new FileInputStream(curFile);
			BufferedInputStream bis = new BufferedInputStream(fs);
			if(extension.contains("docx")){
				XWPFDocument doc = new XWPFDocument(bis);
				XWPFWordExtractor we =  new XWPFWordExtractor(doc);
				documentArea.setText(we.getText());
			}
			else {
				HWPFDocument doc = new HWPFDocument(bis);
				WordExtractor we =  new WordExtractor(doc);
				documentArea.setText(we.getText());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Sets up documentArea based on PDF document that was opened
	 */
	public void openPDFFile(){
		try {
			documentArea.setText("");
			PDDocument doc = PDDocument.load(curFile); 
			PDFTextStripper textStripper=new PDFTextStripper();
			documentArea.setText(textStripper.getText(doc));
			 PDDocumentInformation info = doc.getDocumentInformation();
			 System.out.println( "Page Count=" + doc.getNumberOfPages() );
			 System.out.println( "Title=" + info.getTitle() );
			 System.out.println( "Author=" + info.getAuthor() );
			 System.out.println( "Subject=" + info.getSubject() );
			 System.out.println( "Keywords=" + info.getKeywords() );
			 System.out.println( "Creator=" + info.getCreator() );
			 System.out.println( "Producer=" + info.getProducer() );
			 System.out.println( "Creation Date=" + info.getCreationDate() );
			 System.out.println( "Modification Date=" + info.getModificationDate());
			 System.out.println( "Trapped=" + info.getTrapped() );    
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the extension of a File
	 * @param file
	 * @return
	 */
	private String getExtension(File file) {
		int dotPos = file.getName().lastIndexOf(".");
		return file.getName().substring(dotPos);
	}
	
	
	public void updateHighlightedText() {
		String text = documentArea.getText().toLowerCase();
		highlighter.removeAllHighlights();
		for(Object phrase : list.getSelectedValues()){
			String curPhrase = (String) phrase;
			int startFrom = 0;
			int size = text.length();
			while(startFrom < size) {
				int start = text.indexOf(curPhrase.toLowerCase(), startFrom);
				if(start == -1) break;
				else {
					try {
						highlighter.addHighlight(start, start+curPhrase.length(), DefaultHighlighter.DefaultPainter);
						startFrom = start+curPhrase.length()+1;
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	
	/**
	 * Generates query from only selected keywords
	 * @throws URISyntaxException 
	 */
	public void generateQueryFromSelected() throws URISyntaxException {
		curQuery = "";
		for(Object phrase : list.getSelectedValues()) {
			curQuery += " " + phrase;
		}
		curQuery = curQuery.trim();
		queryLabel.setText(curQuery);
		
		String url = "https://www.google.com/search?hl=en&noj=1&site=webhp&source=hp&q=" + curQuery + "&oq=" + curQuery;
		url = url.replace(' ', '+');
		uri1 = new URI(url);
		
		String url2 = "http://www.googlescholar.com/scholar?q="+curQuery;
		url2 = url2.replace(' ', '+');
		uri2 = new URI(url2);
		
		
		String url3 = "http://www.youtube.com/results?search_query="+curQuery;
		url3 = url3.replace(' ', '+');
		uri3 = new URI(url3);
		
		String url4 = "http://academic.research.microsoft.com/Detail?query=" + curQuery + "&searchtype=1&s=0";
		url4 = url4.replace(' ', '+');
		uri4 = new URI(url4);
		
		
	}

	/**
	 * Generates query from the top 5 keyword phrases
	 */
	public void generateQueryFromAll() {
		curQuery = "";
		int count = 0;
		for(Object phrase : model.toArray()) {
			count++;
			if(count > 1 && count < 5) curQuery+= ",";
			curQuery += " " + phrase;
			if(count == 5) break;
		}
		curQuery = curQuery.trim();
		queryLabel.setText(curQuery);
	}
	
	/**
	 * GUI CODE, DO NOT TAMPER WITH
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public DocumindGUI() throws IOException, URISyntaxException {
		//DON'T CHANGE!
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		container = frame.getContentPane();
		
		// Create the layout
		GridBagLayout gbl = new GridBagLayout();
		
		// Set layout on container
		container.setLayout(gbl);
		
		// Place a component at cell location (0,0)
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 3;
		gbc.gridheight = 5;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.WEST;
		documentArea = new JTextArea();
		documentArea.setLineWrap(true);
		documentArea.setSelectionColor(Color.YELLOW);
		highlighter = documentArea.getHighlighter();
		scrollPane = new JScrollPane(documentArea);
		gbl.setConstraints(scrollPane, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = GridBagConstraints.RELATIVE;
		gbc.weightx = 1;
		gbc.weighty = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.SOUTHWEST;
		curQuery = "";
		queryLabel = new JLabel(curQuery, JLabel.CENTER);
		queryLabel.setBorder (BorderFactory.createTitledBorder("Current Query"));
		gbl.setConstraints(queryLabel, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.PAGE_START;
		getHighlightedButton = new JButton("Get Highlighted Text");
		getHighlightedButton.addActionListener(new MenuButtonListener());
		gbl.setConstraints(getHighlightedButton, gbc);
		
		gbc.gridy = GridBagConstraints.RELATIVE;
		openFileButton = new JButton("Open File");
		openFileButton.addActionListener(new MenuButtonListener());
		gbl.setConstraints(openFileButton, gbc);
		
		rankHighlightedButton = new JButton("Keywords Of Highlighted Text");
		rankHighlightedButton.addActionListener(new MenuButtonListener());
		gbl.setConstraints(rankHighlightedButton, gbc);
		
		textRankButton = new JButton("Keywords Of Entire Document");
		textRankButton.addActionListener(new MenuButtonListener());
		gbl.setConstraints(textRankButton, gbc);
		
		generateQueryButton = new JButton("Set Query (All KeyWords)");
		generateQueryButton.addActionListener(new MenuButtonListener());
		gbl.setConstraints(generateQueryButton, gbc);
		
		generateSelectQueryButton = new JButton("Set Query (Selected KeyWords)");
		generateSelectQueryButton.addActionListener(new MenuButtonListener());
		gbl.setConstraints(generateSelectQueryButton, gbc);
		
		
		model = new DefaultListModel();
		list = new JList(model);
		list.getSelectionModel().addListSelectionListener(new KeyWordSelectionHandler());
		listScroller = new JScrollPane(list);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridheight = 2;
		gbl.setConstraints(listScroller, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = GridBagConstraints.RELATIVE;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.SOUTHWEST;
		resourcesPanel = new JPanel();
		addButtonsToJPanel();
		gbl.setConstraints(resourcesPanel, gbc);
		
		
		//add components to container, order matters!
		container.add(scrollPane);
		container.add(queryLabel);
		container.add(getHighlightedButton);
		container.add(openFileButton);
		container.add(rankHighlightedButton);
		container.add(textRankButton);
		//container.add(generateQueryButton);
		//container.add(generateSelectQueryButton);
		container.add(listScroller);
		container.add(resourcesPanel);
	}
	
	public void viewGUI(){
		frame.pack();
		frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		frame.setVisible(true);
	}
	
	/**
	 * This handles button actions on the side menu
	 * @author Chris
	 *
	 */
	class MenuButtonListener implements ActionListener {
		  MenuButtonListener() {
		  }

		  public void actionPerformed(ActionEvent e) {
		    if (e.getActionCommand().equals("Get Highlighted Text")) {
		      System.out.println(documentArea.getSelectedText());
		      JOptionPane.showMessageDialog(frame, documentArea.getSelectedText());
		    }
		    else if (e.getActionCommand().equals("Open File")) {
		    	openFile();
		    }
		    else if (e.getActionCommand().equals("Keywords Of Highlighted Text")) {
		    	if(curFile != null) getKeywordsFromText(true);
		    }
		    else if(e.getActionCommand().equals("Keywords Of Entire Document")) {
		    	if(curFile!= null) getKeywordsFromText(false);
		    }
		    else if(e.getActionCommand().equals("Set Query (All KeyWords)")) {
		    	generateQueryFromAll();
		    }
		    else if(e.getActionCommand().equals("Set Query (Selected KeyWords)")) {
		    	try {
					generateQueryFromSelected();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    }
		  }
	}
	
	class KeyWordSelectionHandler implements ListSelectionListener {
	    public void valueChanged(ListSelectionEvent e) {
	        updateHighlightedText();
	    }
	}

	
	public static void main(String args[]) throws IOException, URISyntaxException{
		DocumindGUI docMind = new DocumindGUI();
		docMind.viewGUI();
		while(true){}
	}
}

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import LBJ2.nlp.seg.Token;

public class TextRankGraph {
	private ArrayList<TextRankNode> nodeList;
	HashMap<String, TextRankVertex> vertexMap;
	private ArrayList<TextRankVertex> vertexList;

	public TextRankGraph(ArrayList<TextRankNode> nodeList, int windowSize, String[] lines) {
		this.nodeList = nodeList;
		createEdges(windowSize, lines);
	}

	/**
	 * This function compares all words in the nodelist and sees if their distance is within
	 * the parameter window size. (i.e words must be a max distance of 2 away from each other). 
	 * The edges are stored in each TextRankVertex's neighborsList
	 * @param windowSize
	 * @param lines
	 */
	private void createEdges(int windowSize, String[] lines) {
		vertexMap = new HashMap<String, TextRankVertex>();
		int numOfVertices = nodeList.size();
		for(int i = 0; i < numOfVertices-1; i++){
			TextRankNode curNode = nodeList.get(i);
			if(vertexMap.containsKey(curNode.getForm()) == false) {
				TextRankVertex newVertex = new TextRankVertex(curNode);
				vertexMap.put(curNode.getForm(), newVertex);
			}
			TextRankVertex curVertex = vertexMap.get(curNode.getForm());
			for(int j=i+1; j < numOfVertices; j++){
				int distance = 0;
				TextRankNode next = nodeList.get(j);
				if(vertexMap.containsKey(next.getForm()) == false) {
					TextRankVertex newVertex = new TextRankVertex(next);
					vertexMap.put(next.getForm(), newVertex);
				}
				TextRankVertex nextVertex = vertexMap.get(next.getForm());
				distance = next.getPosition() - curNode.getPosition();
				if((distance < windowSize) && (curVertex.neighborsList.contains(nextVertex) == false) && (curVertex.getForm().equalsIgnoreCase(next.getForm()) == false)) {
					System.out.println(distance + " putting together: " + curNode.getForm() + "-" + next.getForm());
					curVertex.neighborsList.add(nextVertex);
					nextVertex.neighborsList.add(curVertex);
				}
				else break;
			}
		}
		
		vertexList = new ArrayList<TextRankVertex>();
		Set<String> keySet = vertexMap.keySet();
		for(String key : keySet){
			vertexList.add(vertexMap.get(key));
		}
		
	}
	
	public ArrayList<TextRankVertex> getVertices(){
		return vertexList;
	}
}

import java.util.Scanner;


public class KeyPhraseNode {
	int count;
	String phrase;
	int score;
	int keyPhraseSize;

	public KeyPhraseNode(String word, int num) {
		this.phrase = word;
		this.count = num;
		this.keyPhraseSize = countWordsInPhrase();
		this.score = count*keyPhraseSize;
	}
	
	public int countWordsInPhrase(){
		String arr[]=phrase.split(" ");
		return arr.length;
	}

}

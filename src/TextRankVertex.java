import java.util.ArrayList;

import LBJ2.nlp.seg.Token;


public class TextRankVertex {
	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public int getIsMarked() {
		return isMarked;
	}

	public void setMarked() {
		this.isMarked = 1;
	}
	
	public String getForm() {
		return word.form.toLowerCase();
	}

	private Token word;
	private String type;
	private double score;
	ArrayList<TextRankVertex> neighborsList;
	private int isMarked;

	public TextRankVertex(TextRankNode curNode) {
		this.word = curNode.getWord();
		this.type = curNode.getType();
		this.score = 1.0;
		this.neighborsList = new ArrayList<TextRankVertex>();
		this.isMarked = 0;
	}

	public ArrayList<TextRankVertex> getNeighborsList() {
		// TODO Auto-generated method stub
		return neighborsList;
	}

}

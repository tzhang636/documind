import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import LBJ2.nlp.SentenceSplitter;
import LBJ2.nlp.WordSplitter;
import LBJ2.nlp.seg.PlainToTokenParser;
import LBJ2.nlp.seg.Token;

import edu.illinois.cs.cogcomp.lbj.pos.POSTagPlain;
import edu.illinois.cs.cogcomp.lbj.pos.POSTagger;

public class TextRank {
	private static final int CO_OCC_WINDOWSIZE = 2; //the window size of how close words need to be together in order to be a neighbors
	private static final int CONVERGE_ITERATIONS = 30; //the # of scoring iterations
	private static final int KEYWORD_LIMIT = 5; //the max size num of marked words 
	private static final double D = 0.85; //the parameter D used in scoring a vertex
	private static final int KEYPHRASE_LIMIT = 3; //the max size of a keyphrase
	
	
	/**
	* Returns a HashMap of parsed words with key as token and value as noun or adjective.
	*/
	public static ArrayList<TextRankNode> getPartOfSpeechTagAdjNounOnly(String[] lines){
		POSTagger tagger = new POSTagger();
		PlainToTokenParser parser = new PlainToTokenParser( new WordSplitter( new SentenceSplitter(lines)));
		Token word;
		ArrayList<TextRankNode> nodeList = new ArrayList<TextRankNode>();
		int position = 1;
		while((word =  (Token) parser.next()) != null){
			String type = tagger.discreteValue(word);
			System.out.println(word.form + ": " + type);
			if(type.contains("NN") || type.contains("JJ")){
				TextRankNode temp = new TextRankNode(word, type, position);
				nodeList.add(temp);
			}
			position++;
		}
		
		return nodeList;
	}


	/**
	 * Runs the TextRank algorithm on the String[] lines, where each value in the array is a 
	 * line from the text. A POS Tagger is performed on the text and returns all the 
	 * words that are nouns or adjectives. Then a graph is created from that list (uses those
	 * nodes as vertices), the vertices are scored, sorted and the top keywords are marked and return
	 * in a Set. This markedWorks set is then used along with the graph and lines String[] to 
	 * get the keyphrases in the text. The keyphrases are then returned.
	 * @param lines
	 * @return
	 */
	public static ArrayList<KeyPhraseNode> runTextRank(String[] lines){
		ArrayList<TextRankNode> nodeList = getPartOfSpeechTagAdjNounOnly(lines);
		TextRankGraph graph = new TextRankGraph(nodeList, CO_OCC_WINDOWSIZE, lines);
		scoreVertices(graph);
		printGraphScores(graph);
		sortVerticesList(graph.getVertices());
		printGraphScores(graph);
		Set<String> markedWords = markTopTKeywords(graph);
		System.out.println("marked words "  + markedWords);
		ArrayList<KeyPhraseNode> keyPhrases = generateKeyPhrases(graph, lines, markedWords);
		return keyPhrases;
	}

	/**
	 * This function generates keyWords based on the markedWorks set by finding marked words that are
	 * next to each other.
	 * @param graph
	 * @param lines
	 * @param markedWords
	 * @return
	 */
	private static ArrayList<KeyPhraseNode> generateKeyPhrases(TextRankGraph graph, String[] lines, Set<String> markedWords) {
		Set<String> keyPhrases = new HashSet<String>();
		HashMap<String, Integer> keyPhrasesCount = new HashMap<String, Integer>();
		for (int i = 0; i < lines.length; i++){
			String curLine = lines[i];
			//System.out.println("+++++" + curLine);
			String[] words = curLine.split("[\\s\\p{Punct}]+");
			String temp = "";
			int keyPhraseSize = 0;
			boolean flag = false;
			for(int j = 0; j < words.length; j++){
				//System.out.println("\t" + "word: " + words[j]);
				if(markedWords.contains(words[j].toLowerCase())){
					flag = true;
				}
				if(flag && graph.vertexMap.containsKey(words[j].toLowerCase())){
					temp = temp + " " + words[j];
					keyPhraseSize++;
				}
				else{
					//System.out.println("word caused phrase to stop: " + words[j]);
					flag = false;
				}
				if(keyPhraseSize == KEYPHRASE_LIMIT || flag == false){
					flag = false;
					String phrase = temp.toLowerCase().trim();
					if(keyPhrasesCount.containsKey(phrase) && keyPhraseSize > 1){
						keyPhrasesCount.put(phrase, keyPhrasesCount.get(phrase)+1);
					}
					else {
						keyPhrasesCount.put(phrase, 1);
					}
					temp = "";
					keyPhraseSize = 0;
				}
			}
		}
		
		//This is just sorting the keyPhrase list by score
		ArrayList<KeyPhraseNode> keyPhraseList = new ArrayList<KeyPhraseNode>();
		for(String phrase : keyPhrasesCount.keySet()){
			KeyPhraseNode temp = new KeyPhraseNode(phrase, keyPhrasesCount.get(phrase));
			keyPhraseList.add(temp);
		}
		Collections.sort(keyPhraseList, (new TextRank()).new KeyPhraseComparator());

		return keyPhraseList;	
	}

	/**
	 * This function marks the top (1/3) or top 5 (which ever is higher) words as markedWords.
	 * @param graph
	 * @return
	 */
	private static Set<String> markTopTKeywords(TextRankGraph graph) {
		ArrayList<TextRankVertex> verticesList = graph.getVertices();
		Set<String> markedWords = new HashSet<String>();
		int max = (verticesList.size()/3);
		System.out.println("MAX HERE:" + max);
		for(int i = 0; i < Math.max(max, KEYWORD_LIMIT) && i < verticesList.size(); i++){
			verticesList.get(i).setMarked();
			markedWords.add(verticesList.get(i).getForm());
		}
		return markedWords;
		
	}

	
	/**
	 * This function sorts the TextRankVertices by their score
	 * @param vertices
	 */
	private static void sortVerticesList(ArrayList<TextRankVertex> vertices) {
		Collections.sort(vertices, (new TextRank()).new TextRankVertexComparator());
	}

	
	/**
	 * This function scores all the vertices continuously, and converges within 30 iterations.
	 * @param graph
	 */
	private static void scoreVertices(TextRankGraph graph) {
		ArrayList<TextRankVertex> vertexList = graph.getVertices();
		//change this later to converge till certain threshold
		for(int i = 0; i < CONVERGE_ITERATIONS; i++){
			int numOfVertices = vertexList.size();
			for(int j = 0; j < numOfVertices; j++){
				scoreVertex(vertexList.get(j));
			}
		}
	}
	
	
	/**
	 * Prints the scores of the vertices in the graph.
	 * @param graph
	 */
	public static void printGraphScores(TextRankGraph graph){
		ArrayList<TextRankVertex> vertexList = graph.getVertices();
		
		System.out.println("Printing Graph Scores");
		
		int numOfVertices = vertexList.size();
		for(int i = 0; i < numOfVertices; i++){
			System.out.println("\t" + vertexList.get(i).getForm() + " : " + vertexList.get(i).getScore());
		}
	}
	

	/**
	 * This function follows the formula in the TextRank algorithm:
	 * http://acl.ldc.upenn.edu/acl2004/emnlp/pdf/Mihalcea.pdf
	 * to score the vertices based on their neighbors.
	 * @param textRankVertex
	 */
	private static void scoreVertex(TextRankVertex textRankVertex) {
		ArrayList<TextRankVertex> neighborsList = textRankVertex.getNeighborsList();
		//System.out.println(textRankVertex.getForm()  + " has these neighbors : " + neighborsList.toString());
		double newScore = 0.0;
		double summationValue = 0.0;
		
		//System.out.println(vertex.getForm() + " has " + neighborsList.size() + " neighbors.");
		//System.out.println("\t" + vertex.getScore());
		for(int i = 0; i < neighborsList.size(); i++){
			TextRankVertex vertexJ = neighborsList.get(i);
			double temp = ((1.0/(vertexJ.getNeighborsList().size())) * vertexJ.getScore());
			summationValue = summationValue + temp;
		}
		
		if(summationValue == 0.0) System.out.println(textRankVertex.getForm() + " has zero sumvalue!");
		//summationValue can be zero here, change?
		newScore = (1.0 - D) + D*summationValue;
		textRankVertex.setScore(newScore);
	}
	
	public class TextRankVertexComparator implements Comparator<TextRankVertex> {

		@Override
		public int compare(TextRankVertex arg0, TextRankVertex arg1) {
			if(arg0.getScore() > arg1.getScore()) return -1;
			else if(arg0.getScore() < arg1.getScore()) return 1;
			else return 0;
		}
		
	}
	
	public class KeyPhraseComparator implements Comparator<KeyPhraseNode> {

		@Override
		public int compare(KeyPhraseNode arg0, KeyPhraseNode arg1) {
			if(arg0.score > arg1.score) return -1;
			else if(arg0.score < arg1.score) return 1;
			else return 0;
		}
		
	}
	
}

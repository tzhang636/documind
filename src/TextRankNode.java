import java.util.ArrayList;
import LBJ2.nlp.seg.Token;

public class TextRankNode {
	private Token word;
	private String type;
	private int position;

	public TextRankNode(Token word, String type, int position) {
		this.word = word;
		this.type = type;
		this.position = position;
	}
	
	public String toString(){
		String ret = "[" + position + " : " + word.form + " : " + type + "]";
		return ret;
	}
	
	public Token getWord() {
		return word;
	}

	public String getForm(){
		return word.form.toLowerCase();
	}

	public String getType() {
		return type;
	}


	public int getPosition() {
		return position;
	}
}
